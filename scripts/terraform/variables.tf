variable "id_rsa_pub" {
    description = "rsa key for digitalocean"
    default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDuJs/Jy1uDaxH+0tYaSd1KqWkBzZficxGaud1+hcJCVwCxRQtA0E3ideA6C2sjSLpb5ASupURbeW2je4zIGU+0TgcVFfapw24NwAkhuw0/zw4VC6Q+KQfhNC+BTf/rCy2drZxzkgG72dL3ZT9oK6Ll+rPfUKw/Jt5DZqLChrMyfvRRzFZtTjJvl+6R/r5J0ONjvecfjSNpnRubddG1YPK5dun0Pn0GhK/xxkcwD5Ktxd0mmgWcc3KJEAAqs3l/G56MmOkOQuRnY1cWT1Km0tEF/+SYXDJyufEoZ95ZLhpibX3zMJcq9fhzkawlineoe/AZbue2e0ZW1hMkcuWBPBVh acpatt@kestrel"
}

variable "secrets" {
  type = map(string)
  sensitive = true
  default = {
  "key" = "secret_Value"
  }
}
  
  
variable "ur_ips" {
  type = list(string)
  default =  [
  "216.144.250.150",
  "69.162.124.226",
  "69.162.124.227",
  "69.162.124.228",
  "69.162.124.229",
  "69.162.124.230",
  "69.162.124.231",
  "69.162.124.232",
  "69.162.124.233",
  "69.162.124.234",
  "69.162.124.235",
  "69.162.124.236",
  "69.162.124.237",
  "63.143.42.242",
  "63.143.42.243",
  "63.143.42.244",
  "63.143.42.245",
  "63.143.42.246",
  "63.143.42.247",
  "63.143.42.248",
  "63.143.42.249",
  "63.143.42.250",
  "63.143.42.251",
  "63.143.42.252",
  "63.143.42.253",
  "216.245.221.82",
  "216.245.221.83",
  "216.245.221.84",
  "216.245.221.85",
  "216.245.221.86",
  "216.245.221.87",
  "216.245.221.88",
  "216.245.221.89",
  "216.245.221.90",
  "216.245.221.91",
  "216.245.221.92",
  "216.245.221.93",
  "46.137.190.132",
  "122.248.234.23",
  "188.226.183.141",
  "178.62.52.237",
  "54.79.28.129",
  "54.94.142.218",
  "104.131.107.63",
  "54.67.10.127",
  "54.64.67.106",
  "159.203.30.41",
  "46.101.250.135",
  "18.221.56.27",
  "52.60.129.180",
  "159.89.8.111",
  "146.185.143.14",
  "139.59.173.249",
  "165.227.83.148",
  "128.199.195.156",
  "138.197.150.151",
  "34.233.66.117"
]
}

variable "domainname" {
  default = "acpatt.com"
}

variable "hostname" {
  default = "mail"
}

variable "all_ips" {
  type = list(string)
  default = ["0.0.0.0/0", "::/0"]
}

locals {
  home_ips    = [data.dns_a_record_set.home.addrs[0]]
  allowed_in  = concat(local.home_ips, var.ur_ips)
  dns_records = [
    {
      name       = "autoconfig"
      content    = digitalocean_droplet.mailserver.ipv4_address
      type       = "A"
    },{
      name       = "autodiscover"
      content    = digitalocean_droplet.mailserver.ipv4_address
      type       = "A"
    },{
      name       = "mta-sts"
      content    = digitalocean_droplet.mailserver.ipv4_address
      type       = "A"
    },{
      name       = var.hostname
      content    = digitalocean_droplet.mailserver.ipv4_address
      type       = "A"
    },{
      name       = "ns1.${var.hostname}"
      content    = "${var.hostname}.${var.domainname}"
      type       = "CNAME"
    },{
      name       = "ns2.${var.hostname}"
      content    = "${var.hostname}.${var.domainname}"
      type       = "CNAME"
    },{
      name       = var.domainname
      content    = "mail.${var.domainname}"
      type       = "MX"
      priority   = 10
    },{
      name       = "_caldavs._tcp"
      type       = "SRV"
      data       = {
        service  = "_caldavs"
        proto    = "_tcp"
        name     = var.domainname
        priority = 0
        weight   = 0
        port     = 443
        target   = "${var.hostname}.${var.domainname}"
      }
    },{
      name       = "_carddavs._tcp"
      type       = "SRV"
      data       = {
        service  = "_carddavs"
        proto    = "_tcp"
        name     = var.domainname
        priority = 0
        weight   = 0
        port     = 443
        target   = "${var.hostname}.${var.domainname}"
      }
    },{
      name       = var.domainname
      content    = "v=spf1 a:${var.hostname}.${var.domainname} -all"
      type       = "TXT"
    },{
      name       = "_dmarc"
      content    = "v=DMARC1; p=quarantine"
      type       = "TXT"
    },{
      name       = "${var.hostname}._domainkey"
      content    = "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvPsl0LriU7n5ToaNloS94L3kbKjqy/bFbJrKAJN0LGCQDe8csh3WqZ4rkNFJ8O0A2KvEpDrltk4CfF3UmpRicKA4UeHZVnmQC91GrXyR6PSiB//rUQFhjmprC0N905ueKNuvkKApMqtoBEmy3M3DfLuGEh65XPgYx9suT9sxRBtaadDkRoPBmSi4d2gYIUKMRejjjWiN1maNE6kuE+Z0CgO0/LxtixbrWDWgCxV3VmB8LK5c67m30K75gqb3e3e6aYr54vGpdHr+n8kpB41ZRQXaKFBZOyHK9I1M530HYsUf7BrMtXELtPO7qaFSmnJmLqsBaEcJ99hN7gGVQJTOcwIDAQAB"
      type       = "TXT"
    }
  ]
  fw_rules    = [
    {
      name             = "Allow my hmoe IP to SSH in"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "22"
      addresses        = local.home_ips
      monitor_type     = null
    },{
      name             = "alllow anyone to submit emails"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "25"
      addresses        = var.all_ips
      monitor_type     = "port"
    },{
      name             = "allow me and UR to get to webpage"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "80"
      addresses        = local.allowed_in
      monitor_type     = "http"
    },{
      name             = "allow me and UR to get to https"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "443"
      addresses        = local.allowed_in
      monitor_type     = null
    },{
      name             = "allow me and UR to send mail"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "587"
      addresses        = local.allowed_in
      monitor_type     = "port"
    },{
      name             = "allow me and UR to POP"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "993"
      addresses        = local.allowed_in
      monitor_type     = "port"
    },{
      name             = "allow me and UR to POPSSL"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "995"
      addresses        = local.allowed_in
      monitor_type     = "port"
    },{
      name             = "allow me and UR to dovecot"
      direction        = "inbound"
      protocol         = "tcp"
      port_range       = "4190"
      addresses        = local.allowed_in
      monitor_type     = "port"
    },{ 
      name             = "allow mail to dnstcp"
      direction        = "outbound"
      protocol         = "tcp"
      port_range       = "53"
      addresses        = var.all_ips
      monitor_type     = null
    },{
      name             = "allow mail to anythign"
      direction        = "outbound"
      protocol         = "tcp"
      port_range       = "all"
      addresses        = var.all_ips
      monitor_type     = null
    },{
      name             = "allow mail to NTP"
      direction        = "outbound"
      protocol         = "udp"
      port_range       = "123"
      addresses        = var.all_ips
      monitor_type     = null
    },{
      name             = "allow mail to DNS"
      direction        = "outbound"
      protocol         = "udp"
      port_range       = "53"
      addresses        = var.all_ips
      monitor_type     = null
    },{
      name             = "allow mail to ping"
      direction        = "outbound"
      protocol         = "icmp"
      port_range       = null
      addresses        = var.all_ips
      monitor_type     = null
    }
    ]
}


