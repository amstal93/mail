terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/22057286/terraform/state/mail"
    lock_address = "https://gitlab.com/api/v4/projects/22057286/terraform/state/mail/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/22057286/terraform/state/mail/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}